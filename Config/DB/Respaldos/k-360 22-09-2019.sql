-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 22-09-2019 a las 14:50:36
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `api-k360`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `Id` int(11) NOT NULL,
  `Categoria_Name` varchar(50) NOT NULL,
  `Id_TorneoF` int(25) NOT NULL,
  `Id_LigaF` int(10) NOT NULL,
  `Total_Equipos` int(5) NOT NULL,
  `Total_Jugadores` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`Id`, `Categoria_Name`, `Id_TorneoF`, `Id_LigaF`, `Total_Equipos`, `Total_Jugadores`) VALUES
(1, 'Categoria A', 5, 2, 17, 2500),
(2, 'Categoria B', 4, 2, 25, 2875),
(3, 'Categoria S1', 7, 1, 0, 0),
(4, 'Categoria S2', 7, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada_resultado`
--

CREATE TABLE `jornada_resultado` (
  `Id` int(11) NOT NULL,
  `Id_Jornada` int(11) NOT NULL,
  `Id_Liga` int(11) NOT NULL,
  `Temporada_IdF` int(10) NOT NULL,
  `Id_Torneo` int(11) NOT NULL,
  `Id_Categoria` int(11) NOT NULL,
  `Temporada` varchar(25) NOT NULL,
  `Id_Encuentro` int(11) NOT NULL,
  `Fecha_Encuentro` date NOT NULL,
  `Hora_Encuentro` time NOT NULL,
  `Id_Equipo_local` int(11) NOT NULL,
  `Nombre_Equipo_local` varchar(50) NOT NULL,
  `Score_Equipo_local` int(11) NOT NULL,
  `Escudo_Equipo_local` varchar(150) NOT NULL,
  `Id_Equipo_Visitante` int(11) NOT NULL,
  `Nombre_Equipo_Visitante` varchar(50) NOT NULL,
  `Score_Equipo_Visitante` int(11) NOT NULL,
  `Escudo_Equipo_Visitante` varchar(150) NOT NULL,
  `Status_Encuentro` varchar(25) NOT NULL,
  `IsActual` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `jornada_resultado`
--

INSERT INTO `jornada_resultado` (`Id`, `Id_Jornada`, `Id_Liga`, `Temporada_IdF`, `Id_Torneo`, `Id_Categoria`, `Temporada`, `Id_Encuentro`, `Fecha_Encuentro`, `Hora_Encuentro`, `Id_Equipo_local`, `Nombre_Equipo_local`, `Score_Equipo_local`, `Escudo_Equipo_local`, `Id_Equipo_Visitante`, `Nombre_Equipo_Visitante`, `Score_Equipo_Visitante`, `Escudo_Equipo_Visitante`, `Status_Encuentro`, `IsActual`) VALUES
(1, 1, 1, 3, 5, 1, '2018-2019', 1, '2019-06-24', '17:00:00', 1, 'Emelec', 2, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/1.png', 2, 'Barcelona', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/2.png', 'Closed', 1),
(2, 1, 3, 3, 5, 1, '2019-2020', 25, '2019-06-24', '18:00:00', 3, 'Delfin', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/3.png', 4, 'Liga Universitaria', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/4.png', 'Closed', 1),
(3, 12, 3, 0, 7, 3, '2019-2020', 26, '2019-06-27', '13:00:00', 5, 'Aucas', 3, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/5.png', 6, 'Nacional', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/6.png', 'Pendiente', 1),
(4, 1, 1, 3, 5, 1, '2018-2019', 4, '2019-06-25', '05:30:00', 7, 'Macara', 0, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/7.png', 8, 'Deportivo olmedo', 0, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/8.png', 'Finalizado', 1),
(5, 2, 1, 3, 5, 1, '2018-2019', 5, '2019-06-25', '13:00:00', 9, 'Deportivo Cuenca', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/9.png', 10, 'Independiente del Valle', 2, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/10.png', 'Finalizado', 0),
(6, 1, 1, 3, 5, 1, '2018-2019', 6, '2019-06-27', '08:00:00', 11, 'Fuerza Amarilla', 2, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/11.png', 12, 'Guayaquil City', 2, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/12.png', 'Pendiente', 1),
(7, 8, 3, 5, 1, 1, '2017-2018', 7, '2019-06-26', '17:15:00', 13, 'Deportivo America ', 3, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/13.png', 14, 'Mushuc Runa Sporting', 4, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/14.png', 'Finalizado', 1),
(8, 2, 1, 3, 5, 1, '2018-2019', 15, '2019-06-26', '13:00:00', 2, 'Barcelona', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/2.png', 9, 'Deportivo Cuenca', 3, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/9.png', 'Pendiente', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada_torneo`
--

CREATE TABLE `jornada_torneo` (
  `Id` int(11) NOT NULL,
  `Id_Liga` int(11) NOT NULL,
  `Id_TemporadaF` int(10) NOT NULL,
  `Id_Torneo` int(11) NOT NULL,
  `Id_categoria` int(11) NOT NULL,
  `Temporada` varchar(25) NOT NULL,
  `Jornada_numero` int(11) NOT NULL,
  `Desde` date NOT NULL,
  `Hasta` date NOT NULL,
  `Encuentros_Disputados_Jornada` int(11) NOT NULL,
  `Status_jornada` varchar(25) NOT NULL,
  `IsActual` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `jornada_torneo`
--

INSERT INTO `jornada_torneo` (`Id`, `Id_Liga`, `Id_TemporadaF`, `Id_Torneo`, `Id_categoria`, `Temporada`, `Jornada_numero`, `Desde`, `Hasta`, `Encuentros_Disputados_Jornada`, `Status_jornada`, `IsActual`) VALUES
(1, 1, 3, 5, 1, '2018-2019', 1, '2018-08-17', '2018-08-20', 5, 'Closed', 0),
(2, 1, 3, 5, 1, '2018-2019', 2, '2018-08-24', '2018-08-27', 6, 'Closed', 0),
(3, 1, 3, 5, 1, '2018-2019', 3, '2018-08-31', '2018-09-24', 5, 'Closed', 0),
(4, 0, 0, 1, 1, '2018-2019', 4, '2018-09-14', '2018-09-17', 6, 'Closed', 0),
(5, 1, 3, 5, 2, '2018-2019', 5, '2019-07-01', '2019-07-01', 85, 'Open', 0),
(6, 1, 0, 0, 1, '2018-2019', 0, '0000-00-00', '0000-00-00', 0, '', 0),
(7, 0, 0, 2, 1, '2017-2018', 0, '0000-00-00', '0000-00-00', 0, '', 0),
(8, 0, 0, 2, 1, '2017-2018', 0, '0000-00-00', '0000-00-00', 0, '', 0),
(10, 0, 0, 2, 1, '2017-2018', 0, '0000-00-00', '0000-00-00', 0, '', 0),
(11, 3, 0, 7, 3, '2019-2020', 1, '2019-09-01', '2019-09-03', 4, 'Open', 1),
(12, 3, 5, 7, 3, '2019-2020', 2, '2019-09-04', '2019-09-07', 15, 'Open', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `liga`
--

CREATE TABLE `liga` (
  `Id_Liga` int(11) NOT NULL,
  `Nombre_Liga` varchar(50) NOT NULL,
  `Id_PaisF` int(10) NOT NULL,
  `paisnameF` varchar(100) NOT NULL,
  `banderapais` varchar(200) NOT NULL,
  `Id_EstadoF` int(10) NOT NULL,
  `EstadonameF` varchar(100) NOT NULL,
  `Id_MunicipioF` int(10) NOT NULL,
  `MunicipioNameF` varchar(100) NOT NULL,
  `Id_Parroquia` int(10) NOT NULL,
  `ParroquiaNameF` varchar(100) NOT NULL,
  `Descripcion` text NOT NULL,
  `Fundacion` varchar(10) NOT NULL,
  `PresidenteLiga` varchar(100) NOT NULL,
  `VicePresidneteLiga` varchar(100) NOT NULL,
  `SecretarioLiga` varchar(100) NOT NULL,
  `Tlf1` varchar(25) NOT NULL,
  `Tlf2` varchar(25) NOT NULL,
  `Total_Categorias` int(10) NOT NULL,
  `Total_Equipos` int(10) NOT NULL,
  `Total_Jugadores` int(10) NOT NULL,
  `Url_Escudo` varchar(150) NOT NULL,
  `Facebook` varchar(150) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `instagram` varchar(150) NOT NULL,
  `youtube` varchar(150) NOT NULL,
  `IsActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `liga`
--

INSERT INTO `liga` (`Id_Liga`, `Nombre_Liga`, `Id_PaisF`, `paisnameF`, `banderapais`, `Id_EstadoF`, `EstadonameF`, `Id_MunicipioF`, `MunicipioNameF`, `Id_Parroquia`, `ParroquiaNameF`, `Descripcion`, `Fundacion`, `PresidenteLiga`, `VicePresidneteLiga`, `SecretarioLiga`, `Tlf1`, `Tlf2`, `Total_Categorias`, `Total_Equipos`, `Total_Jugadores`, `Url_Escudo`, `Facebook`, `twitter`, `instagram`, `youtube`, `IsActive`) VALUES
(1, 'Liga Barrial La Argelia', 2, 'Ecuador', 'http://localhost/Api-360/AssetDemo/Pais/ecuador.jpg', 1, 'Guayas', 1, 'Guayaquil', 1, 'Las palmas', 'El fútbol es un deporte que se juega de once jugadores por equipo , diez jugadores y un arquero por equipo', '2012', 'Pedro Camejo', 'Juan Carrizal', 'Miguel Bolivar', '5555-5555-555', '4444-444-444', 7, 42, 785, 'http://localhost/Api-360/AssetDemo/Escudo-Liga/ligaDemo1.jpg', '', '', '', '', 1),
(2, 'Liga Barrial Santa Lucia', 2, 'Ecuador', 'http://localhost/Api-360/AssetDemo/Pais/ecuador.jpg', 2, 'Pichincha', 2, 'Quito', 2, 'Carcelen', 'A pocas horas de comenzar un nuevo año deportivo, nos encontramos con la competencia del ‘rally Dakar’ en Argentina, Chile y Perú; tomamos contacto con la incansable NBA que se juega prácticamente todos los días; abrazamos el fin de la parada invernal en el viejo continente; aplaudimos de pie a la federación inglesa de fútbol por brindarnos partidos en fechas festivas; y, entre otras cosas, esperamos con ansias el inicio de un nueva temporada ATP con el primer Grand Slam en Melbourne, Australia; la Copa de África que se desarrollará en Gabón y Guinea Ecuatorial (país no democrático, por cierto); y la Eurocopa', '1989', 'Julia perales', 'Jose almibar', 'Carlos Quiñones', '77777-7777-777', '7777-2222-444', 8, 52, 956, 'http://localhost/Api-360/AssetDemo/Escudo-Liga/ligaDemo2.png', '', '', '', '', 1),
(3, 'Liga Francesa', 4, 'Francia', 'http://localhost/Api-360/AssetDemo/Pais/Francia.jpg', 15, 'Lafra', 15, 'Lafre', 15, 'cafre', 'Liga democratica de francsa del alto y bajos fanatico', '1895', 'casi k', 'medoza', 'josefina', '7777777', '77777777', 5, 78, 1254, 'http://localhost/Api-360/AssetDemo/Escudo-Liga/ligaDemo3.jpg', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticiast`
--

CREATE TABLE `noticiast` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(150) NOT NULL,
  `PrincipalPublicacion` int(1) NOT NULL,
  `IdLigaF` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `noticiast`
--

INSERT INTO `noticiast` (`id`, `titulo`, `descripcion`, `imagen`, `PrincipalPublicacion`, `IdLigaF`) VALUES
(1, 'El asistente de Google lee los textos ', 'Si un usuario está conduciendo o montado en una bici y recibe una notificación, su terminal puede leer en voz alta todos los mensajes. Google ha actualizado su asistente de voz en su versión en inglés y, desde ahora, permite a sus usuarios pedirle que lea los mensajes que han recibido en diversas aplicaciones de mensajería como WhatsApp, Telegram o Messenger, Slack, Discord y GroupMe.', 'http://localhost/kornner-360%20-v2/V2/360/noticias1.jpg', 0, 2),
(2, 'Conceptos de inteligencia artificial', 'La IA está cada vez más presente en nuestro día a día: a medida que aumenta su ámbito de uso, tanto nosotros como las grandes empresas o los gobiernos pasamos a depender más de esta tecnología. Y, a medida que esto ocurre, nos vemos obligados a valorar no sólo su funcionalidad, sino también su seguridad: ¿Qué probabilidad existe de que la IA falle o, peor aún, de que sea vulnerable a un ataque?', 'http://localhost/kornner-360%20-v2/V2/360/noticias2.jpg', 0, 2),
(3, 'Probamos el fútbol de Mitele Plus', 'Una de las sorpresas veraniegas, además del resurgir de Bitcoin hasta los 12.000 euros en julio, ha sido la presentación de Mitele Plus incluyendo el fútbol de LaLiga y la Champions League en la plataforma de Mediaset. La nueva única vía para ver \"todo\" el fútbol independientemente de cuál sea nuestra operadora', 'http://localhost/kornner-360%20-v2/V2/360/noticias3.jpg', 0, 2),
(4, 'civilización depende igual software que del agua', 'Si hubiera un premio para la persona que más influye en nuestras vidas sin que casi nadie le conozca, Bjarne Stroustrup sería un buen candidato. No solo Stroustrup es anónimo para el público, también su gran creación –el lenguaje de programación C++– es desconocida fuera de la informática. Pero está en todas partes: \"La mejor aproximación es decir que hoy todo el mundo lo ha usado y mucha gente lo usa todo el rato. Y nunca lo ves\", dice Stroustrup.', 'http://localhost/kornner-360%20-v2/V2/360/noticias4.jpg', 1, 2),
(5, 'Noticia test', 'Somos Una Organizacion formada por profesionales en analisis de Datos (Big Data), Tenemos como Objetivo Promover Ligas y Jugadores Mediante una plataforma web-services', 'http://localhost/kornner-360%20-v2/V2/360/noticias3.jpg', 0, 2),
(6, 'Santa Lucia N1', '¿Qué es Lorem Ipsum?\r\nLorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas \"Letraset\", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.', 'http://localhost/kornner-360/v2-2/360/ns1.jpeg', 0, 1),
(7, 'Santa Lucia N', '¿Qué es Lorem Ipsum?\r\nLorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas \"Letraset\", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.', 'http://localhost/kornner-360/v2-2/360/ns2.jpg', 0, 1),
(8, 'Santa Lucia N3', '¿Qué es Lorem Ipsum?\r\nLorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas \"Letraset\", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.', 'http://localhost/kornner-360/v2-2/360/ns3.jpg', 0, 1),
(9, 'Santa Lucia N4', '¿Qué es Lorem Ipsum?\r\nLorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas \"Letraset\", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.', 'http://localhost/kornner-360/v2-2/360/ns5.jpg', 0, 1),
(10, 'Santa Lucia N5', '¿Qué es Lorem Ipsum?\r\nLorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas \"Letraset\", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.', 'http://localhost/kornner-360/v2-2/360/istockphoto-957134054-1024x1024.jpg', 0, 1),
(11, 'Santa Lucia N6', '¿Qué es Lorem Ipsum?\r\nLorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas \"Letraset\", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.', 'http://localhost/kornner-360/v2-2/360/nsPrincipal.png', 1, 1),
(12, 'principal', '¿Qué es Lorem Ipsum? Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en ', 'http://localhost/kornner-360/v2-2/360/NoticiaPrincipal.jpg', 1, 0),
(13, 'Noticia Francesa', 'La estrategia es el complemento de la táctica, es decir cómo se van a mover los jugadores o cómo van a reaccionar ante unas situaciones específicas. Este es un concepto dinámico que engloba los movimientos individuales y colectivos y la filosofía de juego.', 'http://localhost/Api-360/AssetDemo/Noticias/noticias-Francesa.jpg', 1, 3),
(14, 'Tottenham y el Arsenal ', 'El Tottenham y el Arsenal se verán las caras este domingo en el derbi de Londres y en el que está puesta mucha expectación en lo que pueda hacer Harry Kane, que se deberá enfrentar a David Luiz, desafortunado en el último duelo ante el Liverpool.', 'http://localhost/Api-360/AssetDemo/Noticias/noticias-Francesa2.jpg', 0, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_posiciones`
--

CREATE TABLE `tabla_posiciones` (
  `Id` int(11) NOT NULL,
  `Id_Liga` int(25) NOT NULL,
  `Id_TemporadaF` varchar(10) NOT NULL,
  `Id_TorneoF` int(10) NOT NULL,
  `Id_Categoria` int(25) NOT NULL,
  `Nombre_Liga` varchar(50) NOT NULL,
  `Nombre_Categoria` varchar(50) NOT NULL,
  `Temporada` varchar(25) NOT NULL,
  `Id_Equipo` int(25) NOT NULL,
  `Nombre_Equipo` varchar(50) NOT NULL,
  `JJ` int(5) NOT NULL,
  `JG` int(5) NOT NULL,
  `JE` int(5) NOT NULL,
  `JP` int(5) NOT NULL,
  `GF` int(5) NOT NULL,
  `GC` int(5) NOT NULL,
  `GD` int(5) NOT NULL,
  `Pts` int(10) NOT NULL,
  `Racha` varchar(10) NOT NULL,
  `Total_Tamarilla` int(11) NOT NULL,
  `Total_Troja` int(11) NOT NULL,
  `Total_Tarjetas` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL,
  `ImagenEscudo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tabla_posiciones`
--

INSERT INTO `tabla_posiciones` (`Id`, `Id_Liga`, `Id_TemporadaF`, `Id_TorneoF`, `Id_Categoria`, `Nombre_Liga`, `Nombre_Categoria`, `Temporada`, `Id_Equipo`, `Nombre_Equipo`, `JJ`, `JG`, `JE`, `JP`, `GF`, `GC`, `GD`, `Pts`, `Racha`, `Total_Tamarilla`, `Total_Troja`, `Total_Tarjetas`, `IsActive`, `ImagenEscudo`) VALUES
(1, 1, '2', 7, 1, 'Liga_Demo', 'Categoria_Demo', '2018-2019', 1, 'Barcelona', 38, 26, 9, 3, 90, 36, 54, 87, '2 Ganados', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/liga/Clubes/Espana/BBVA/Icono/3.png'),
(2, 1, '9', 7, 1, 'Liga_Demo', 'Categoria_Demo', '2018-2019', 2, 'Atletico Madrid', 38, 22, 10, 6, 55, 29, 26, 76, '2 Ganados', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/liga/Clubes/Espana/BBVA/Icono/42.png'),
(3, 1, '9', 7, 1, 'Liga_Demo', 'Categoria_Demo', '2018-2019', 3, 'Real Madrid', 38, 21, 5, 12, 63, 46, 17, 68, '1 Empates', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/liga/Clubes/Espana/BBVA/Icono/1.png'),
(4, 1, '9', 7, 1, 'Liga_Demo', 'Categoria_Demo', '2018-2019', 4, 'Valencia C.F.', 38, 15, 16, 7, 51, 35, 16, 61, '2 Perdidos', 8, 3, 11, 1, 'http://localhost/kornner-360/v2-2/360/liga/Clubes/Espana/BBVA/Icono/17.png'),
(5, 1, '9', 7, 1, 'Liga_Demo', 'Categoria_Demo', '2018-2019', 5, 'Gatafe', 38, 15, 14, 9, 48, 35, 13, 59, '1 Empates', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/liga/Clubes/Espana/BBVA/Icono/4.png'),
(6, 1, '2', 7, 1, 'Liga_Demo', 'Categoria_Demo', '2018-2019', 6, 'Sevilla', 38, 17, 8, 13, 62, 47, 15, 59, '1 Ganados', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/liga/Clubes/Espana/BBVA/Icono/53_sevilla.png'),
(7, 1, '2', 7, 4, 'Liga_Demo', 'Categoria_Demo', '2018-2019', 7, 'RCD Espanyol', 38, 14, 11, 13, 48, 50, -2, 53, '2 Empates', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/liga/Clubes/Espana/BBVA/Icono/8.png'),
(8, 1, '2', 7, 4, 'Liga_Demo', 'Categoria_Demo', '2018-2019', 8, 'Ath.Bilbao', 38, 13, 14, 11, 41, 45, -4, 53, '1 Ganados', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/liga/Clubes/Espana/BBVA/Icono/5.png'),
(9, 1, '2', 7, 4, 'Liga_Demo', 'Categoria_Demo', '2018-2019', 9, 'Real Sociedad', 38, 13, 11, 14, 45, 46, -1, 50, '1 Perdidos', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/liga/Clubes/Espana/BBVA/Icono/16.png'),
(10, 1, '2', 7, 1, 'Liga_Demo', 'Categoria_Demo', '2018-2019', 10, 'Betis', 38, 14, 8, 16, 44, 52, -8, 50, '1 Perdidos', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/liga/Clubes/Espana/BBVA/Icono/2.png'),
(11, 3, '5', 0, 3, 'Liga_Demo', 'Categoria_Demo', '2017-2018', 1, 'Barcelona', 38, 28, 9, 1, 99, 29, 70, 93, '1 Ganados', 0, 0, 0, 0, 'Imagenes/Escudo/Barcelona.png'),
(12, 3, '5', 0, 3, 'Liga_Demo', 'Categoria_Demo', '2017-2018', 2, 'Atletico Madrid', 38, 23, 10, 5, 58, 22, 36, 79, '1 Empates', 0, 0, 0, 0, 'Imagenes/Escudo/Atletico Madrid.png'),
(13, 3, '5', 0, 3, 'Liga_Demo', 'Categoria_Demo', '2017-2018', 3, 'Real Madrid', 38, 22, 10, 6, 94, 44, 50, 76, '1 Ganados', 0, 0, 0, 0, 'Imagenes/Escudo/Real Madrid.png'),
(14, 3, '5', 0, 3, 'Liga_Demo', 'Categoria_Demo', '2017-2018', 4, 'Valencia C.F.', 38, 22, 7, 9, 65, 38, 27, 73, '2 Empates', 15, 7, 22, 0, 'Imagenes/Escudo/Valencia.jpg'),
(15, 3, '5', 0, 3, 'Liga_Demo', 'Categoria_Demo', '2017-2018', 11, 'Villarreal', 38, 18, 7, 13, 57, 50, 7, 61, '2 Ganados', 0, 0, 0, 0, 'Imagenes/Escudo/Villarreal.png'),
(16, 3, '', 0, 3, 'Liga_Demo', 'Categoria_Demo', '2017-2018', 10, 'Betis', 38, 18, 6, 14, 60, 61, -1, 60, '1 Empates', 0, 0, 0, 0, 'Imagenes/Escudo/Betis.jpg'),
(17, 0, '2', 7, 4, 'Premier League', 'Primera Division', '2018-2019', 12, 'Manchester City', 38, 32, 2, 4, 95, 23, 72, 98, '5 Ganados', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/imagenes/Clubes/Inglaterra/manc.png'),
(18, 0, '2', 7, 7, 'Premier League', 'Primera Division', '2018-2019', 13, 'Liverpool', 38, 30, 7, 1, 89, 22, 67, 97, '5 Ganados', 12, 8, 20, 1, 'http://localhost/kornner-360/v2-2/360/imagenes/Clubes/Inglaterra/liverpool.jpg'),
(19, 3, '5', 7, 3, 'Premier League', 'Primera Division', '2018-2019', 14, 'Chelsea', 38, 21, 9, 8, 63, 39, 24, 72, '1 Empates', 0, 0, 0, 1, 'http://localhost/kornner-360/v2-2/360/imagenes/Clubes/Inglaterra/chesea.png'),
(20, 1, '3', 5, 1, 'Premier League', 'Primera Division', '2017-2018', 12, 'Manchester City', 38, 32, 4, 2, 106, 27, 79, 100, '2 Ganados', 14, 10, 24, 0, 'http://localhost/Api-360/AssetDemo/Escudo-Liga/manchestercity.jpg'),
(21, 1, '3', 5, 1, 'Premier League', 'Primera Division', '2017-2018', 15, 'Manchester United', 38, 25, 6, 7, 68, 28, 40, 81, '1 Ganados', 15, 6, 9, 0, 'http://localhost/Api-360/AssetDemo/Escudo-Liga/manchesterUnited.jpg'),
(22, 0, '2', 7, 44, 'Premier League', 'Primera Division', '2017-2018', 16, 'Tottenham Hotspur', 38, 23, 8, 7, 74, 36, 38, 77, '2 Ganados', 0, 0, 0, 0, ''),
(23, 0, '2', 7, 57, 'Premier League', 'Primera Division', '2017-2018', 17, 'Liverpool', 38, 21, 12, 5, 84, 38, 46, 75, '1 Ganados', 13, 5, 18, 0, ''),
(24, 3, '', 0, 3, 'Liga Pro', 'Maxima', '2018-2019', 17, 'Barcelona SC', 15, 10, 3, 2, 34, 16, 18, 33, '5 Ganados', 0, 0, 0, 1, ''),
(25, 3, '', 0, 3, 'Liga Pro', 'Maxima', '2018-2019', 18, 'Macara', 15, 9, 5, 1, 25, 8, 17, 32, '1 Empate', 0, 0, 0, 1, ''),
(26, 3, '', 0, 3, 'Liga Pro', 'Maxima', '2017-2018', 19, 'EMELEC', 22, 12, 5, 5, 35, 17, 18, 41, '2 Ganados', 0, 0, 0, 0, ''),
(27, 3, '', 0, 3, 'Liga Pro', 'Maxima', '2017-2018', 18, 'Macara', 22, 10, 8, 4, 31, 24, 7, 38, '1 Perdido', 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temporada`
--

CREATE TABLE `temporada` (
  `Id` int(11) NOT NULL,
  `Temporada` varchar(10) NOT NULL,
  `Id_LigaF` int(10) NOT NULL,
  `Liga_Name` varchar(50) NOT NULL,
  `IsActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `temporada`
--

INSERT INTO `temporada` (`Id`, `Temporada`, `Id_LigaF`, `Liga_Name`, `IsActive`) VALUES
(1, '2018-2019', 2, '', 1),
(2, '2017-2018', 2, '', 0),
(3, '2016-2017', 1, '', 1),
(4, '2015-2016', 1, '', 0),
(5, '2019-2020', 3, '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `titulos`
--

CREATE TABLE `titulos` (
  `Id` int(11) NOT NULL,
  `Id_LigaF` int(11) NOT NULL,
  `Id_categoriaF` int(11) NOT NULL,
  `Id_Equipo` int(11) NOT NULL,
  `Nombre_Equipo` varchar(150) NOT NULL,
  `Escudo_Equipo` varchar(150) NOT NULL,
  `Titulos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `titulos`
--

INSERT INTO `titulos` (`Id`, `Id_LigaF`, `Id_categoriaF`, `Id_Equipo`, `Nombre_Equipo`, `Escudo_Equipo`, `Titulos`) VALUES
(1, 1, 1, 0, 'Chelsea', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/Chelsea.jpg', 10),
(2, 1, 1, 0, 'Manchester United', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/manchesterUnited.jpg', 5),
(3, 1, 1, 0, 'Bayern Munchen', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/bayern%20munchen.png', 3),
(4, 1, 1, 0, 'Barcelona', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/Barcelona.jpg', 7),
(5, 1, 2, 0, 'Emelec', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/Club-Sport-Emelec.png', 15),
(6, 1, 2, 0, 'Delfin', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/Delfin_SC_Logo.png', 7),
(7, 1, 2, 0, 'Aucas', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/Logo_Aucas_2016.png', 11),
(8, 1, 2, 0, 'olmedo', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/ClubCentroDeportivoOlmedo.png', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `torneo`
--

CREATE TABLE `torneo` (
  `Id_Torneo` int(11) NOT NULL,
  `Id_LigaF` int(10) NOT NULL,
  `Temporada_IdF` int(25) NOT NULL,
  `Id_categoriaF` int(10) NOT NULL,
  `Torneo_Name` varchar(50) NOT NULL,
  `Temporada` varchar(25) NOT NULL,
  `Liga_Name` varchar(50) NOT NULL,
  `Name_Categoria` varchar(50) NOT NULL,
  `Total_Equipos_Participantes` int(11) NOT NULL,
  `Total_Jugadores_Participantes` int(11) NOT NULL,
  `Total_Encuentros_Torneo` int(10) NOT NULL,
  `Total_Goles_Torneo` int(10) NOT NULL,
  `Id_Equipo_Campeon` int(10) NOT NULL,
  `Nombre_Campeon` varchar(50) NOT NULL,
  `EscudoEquipoCampeon` varchar(150) NOT NULL,
  `Id_Equipo_Sub-Campeon` int(10) NOT NULL,
  `nombre_Subcampeon` varchar(50) NOT NULL,
  `EscudoEquipoSubCampeon` varchar(150) NOT NULL,
  `Url_Logo` varchar(150) NOT NULL,
  `Is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `torneo`
--

INSERT INTO `torneo` (`Id_Torneo`, `Id_LigaF`, `Temporada_IdF`, `Id_categoriaF`, `Torneo_Name`, `Temporada`, `Liga_Name`, `Name_Categoria`, `Total_Equipos_Participantes`, `Total_Jugadores_Participantes`, `Total_Encuentros_Torneo`, `Total_Goles_Torneo`, `Id_Equipo_Campeon`, `Nombre_Campeon`, `EscudoEquipoCampeon`, `Id_Equipo_Sub-Campeon`, `nombre_Subcampeon`, `EscudoEquipoSubCampeon`, `Url_Logo`, `Is_active`) VALUES
(0, 3, 6, 1, 'S Lucia C', '2019-2020', 'Francesa', 'Francia categoria A', 0, 0, 0, 0, 0, '', '', 0, '', '', '', 0),
(1, 4, 1, 1, 'Torneo A', '2015-2016', '', '', 500, 600, 700, 800, 3, 'Juventus', '', 1, 'Chelsea', '', '', 0),
(2, 1, 3, 1, 'Torneo B', '2016-2017', '', '', 0, 0, 0, 0, 1, 'Chelsea', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/Chelsea.jpg', 2, 'Barcelona', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/Barcelona.jpg', '', 0),
(4, 4, 3, 6, 'Torneo 1', '2017-2018', '', '', 0, 0, 0, 0, 3, 'Juventus', '', 2, 'Barcelona', '', '', 0),
(5, 1, 3, 1, 'Torneo 2', '2018-2019', '', '', 6, 94, 52, 75, 0, 'Chelsea', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/Chelsea.jpg', 0, 'Manchester United ', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/manchesterUnited.jpg', '', 1),
(6, 0, 1, 0, 'S Lucia A', '', '', '', 0, 0, 0, 0, 0, '', '', 0, '', '', '', 0),
(7, 0, 5, 3, 'S Lucia B', '', '', '', 7, 140, 89, 134, 0, '', '', 0, '', '', '', 1),
(8, 3, 5, 8, 'Torneo II', '2019-2020', 'Francesa', 'F categoria B', 13, 285, 32, 56, 0, '', '', 0, '', '', '', 0),
(9, 3, 6, 6, 'Torneo III', '2020-2021', 'Francesa', 'Francia categoria A', 7, 104, 19, 36, 0, '', '', 0, '', '', '', 1),
(10, 1, 3, 1, 'S Lucia B', '2010-2011', '', '', 0, 0, 0, 0, 0, 'bayern munchen', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/bayern%20munchen.png', 0, 'Inter de Milan', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/Inter.png', '', 0),
(11, 1, 3, 1, '', '2012-2013', '', '', 0, 0, 0, 0, 0, 'manchester United', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/manchesterUnited.jpg', 0, 'Liga Pro', 'http://localhost/Api-360/AssetDemo/Escudo-Liga/ligaDemo1.jpg', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `torneo_goleador`
--

CREATE TABLE `torneo_goleador` (
  `Id_TorneoGoleador` int(11) NOT NULL,
  `Id_Liga` int(10) NOT NULL,
  `Temporada_IdF` int(10) NOT NULL,
  `Id_torneoF` int(10) NOT NULL,
  `Id_Categoria` int(10) NOT NULL,
  `Id_JugadorF` int(10) NOT NULL,
  `name_Jugador` varchar(100) NOT NULL,
  `Id_EquipoF` int(10) NOT NULL,
  `Nombre_EquipoF` varchar(100) NOT NULL,
  `GolesAnotados` int(5) NOT NULL,
  `Encuentros_disputados` int(10) NOT NULL,
  `promedioGolMinuto` int(10) NOT NULL,
  `imagenPerfiljugador` varchar(250) NOT NULL,
  `IsActive` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `torneo_goleador`
--

INSERT INTO `torneo_goleador` (`Id_TorneoGoleador`, `Id_Liga`, `Temporada_IdF`, `Id_torneoF`, `Id_Categoria`, `Id_JugadorF`, `name_Jugador`, `Id_EquipoF`, `Nombre_EquipoF`, `GolesAnotados`, `Encuentros_disputados`, `promedioGolMinuto`, `imagenPerfiljugador`, `IsActive`) VALUES
(1, 1, 3, 5, 1, 52, 'Kylian Mbappé', 5, 'Real Sociedad', 17, 21, 87, 'http://localhost/kornner-360/v2-2/360/liga/Asset/Jugadores/kylian.jpg', 1),
(2, 1, 9, 7, 1, 42, 'Gareth Bale', 4, 'Manchester City', 11, 15, 75, 'http://localhost/kornner-360/v2-2/360/liga/Asset/Jugadores/gare.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_pais`
--

CREATE TABLE `t_pais` (
  `Id_Pais` int(11) NOT NULL,
  `Nombre_Pais` varchar(50) NOT NULL,
  `Continente` varchar(50) NOT NULL,
  `Url_Bandera` varchar(150) NOT NULL,
  `adddate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `t_pais`
--

INSERT INTO `t_pais` (`Id_Pais`, `Nombre_Pais`, `Continente`, `Url_Bandera`, `adddate`) VALUES
(1, 'Chile', 'America', 'http://localhost/Api-360/AssetDemo/Pais/Chile.png', '2019-08-27'),
(2, 'Ecuador', 'America', 'http://localhost/Api-360/AssetDemo/Pais/ecuador.jpg', '2019-08-26'),
(3, 'Peru', 'America', 'http://localhost/Api-360/AssetDemo/Pais/peru.png', '2019-08-25'),
(4, 'Francia', 'Europa', 'http://localhost/Api-360/AssetDemo/Pais/Francia.jpg', '2019-08-24'),
(5, 'Venezuela', 'America', 'http://localhost/Api-360/AssetDemo/Pais/venezuela.jpg', '2019-08-23'),
(6, 'Italia', 'Europa', 'http://localhost/Api-360/AssetDemo/Pais/italia.gif', '2019-08-22'),
(7, 'Mexico', 'America', 'http://localhost/Api-360/AssetDemo/Pais/mexico.jpg', '2019-08-21');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `jornada_resultado`
--
ALTER TABLE `jornada_resultado`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `jornada_torneo`
--
ALTER TABLE `jornada_torneo`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `liga`
--
ALTER TABLE `liga`
  ADD PRIMARY KEY (`Id_Liga`);

--
-- Indices de la tabla `noticiast`
--
ALTER TABLE `noticiast`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tabla_posiciones`
--
ALTER TABLE `tabla_posiciones`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `temporada`
--
ALTER TABLE `temporada`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `titulos`
--
ALTER TABLE `titulos`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `torneo`
--
ALTER TABLE `torneo`
  ADD PRIMARY KEY (`Id_Torneo`);

--
-- Indices de la tabla `torneo_goleador`
--
ALTER TABLE `torneo_goleador`
  ADD PRIMARY KEY (`Id_TorneoGoleador`);

--
-- Indices de la tabla `t_pais`
--
ALTER TABLE `t_pais`
  ADD PRIMARY KEY (`Id_Pais`),
  ADD UNIQUE KEY `Id_Pais` (`Id_Pais`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `jornada_resultado`
--
ALTER TABLE `jornada_resultado`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `jornada_torneo`
--
ALTER TABLE `jornada_torneo`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `liga`
--
ALTER TABLE `liga`
  MODIFY `Id_Liga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `noticiast`
--
ALTER TABLE `noticiast`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `tabla_posiciones`
--
ALTER TABLE `tabla_posiciones`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `temporada`
--
ALTER TABLE `temporada`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `titulos`
--
ALTER TABLE `titulos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `torneo_goleador`
--
ALTER TABLE `torneo_goleador`
  MODIFY `Id_TorneoGoleador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `t_pais`
--
ALTER TABLE `t_pais`
  MODIFY `Id_Pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
