-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 11-08-2019 a las 19:23:36
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `k-360`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `Id` int(11) NOT NULL,
  `Categoria_Name` varchar(50) NOT NULL,
  `Id_LigaF` int(10) NOT NULL,
  `Total_Equipos` int(5) NOT NULL,
  `Total_Jugadores` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuentro`
--

CREATE TABLE `encuentro` (
  `Id` int(11) NOT NULL,
  `Temporada` varchar(10) NOT NULL,
  `Id_Torneo` int(10) NOT NULL,
  `Id_LigaF` int(10) NOT NULL,
  `Id_CategoriaF` int(10) NOT NULL,
  `Numero_Vocalia` varchar(25) NOT NULL,
  `Fecha_Encuentro` date NOT NULL,
  `Hora_Encuentro` time NOT NULL,
  `Estadio` varchar(50) NOT NULL,
  `Etapa_Torneo` varchar(50) NOT NULL,
  `Jornada_Torneo` varchar(50) NOT NULL,
  `Id_Anotador` varchar(25) NOT NULL,
  `Name_Anotador` varchar(50) NOT NULL,
  `Id_Arbritro` varchar(50) NOT NULL,
  `Name_Arbitro` varchar(50) NOT NULL,
  `Id_Equipo_Local` int(11) NOT NULL,
  `Nombre_Equipo_Local` varchar(50) NOT NULL,
  `Id_Equipo_Visitante` int(11) NOT NULL,
  `Nombre_Equipo_Visitante` varchar(50) NOT NULL,
  `Goles_Equipo_Local` int(11) NOT NULL,
  `Goles_Equipo_Visitante` int(11) NOT NULL,
  `Tarjeta_Amarilla_Equipo_Local` int(11) NOT NULL,
  `Tarjeta_Amarilla_Equipo_Visitante` int(11) NOT NULL,
  `Tarjeta_Roja_Equipo_Local` int(11) NOT NULL,
  `Tarjeta_Roja_Equipo_Visitante` int(11) NOT NULL,
  `Id_Jugador_Local_1` int(11) NOT NULL,
  `Formacion_Jugador_Local_1` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_1` int(10) NOT NULL,
  `Id_Jugador_Local_2` int(10) NOT NULL,
  `Formacion_Jugador_Local_2` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_2` int(10) NOT NULL,
  `Id_Jugador_Local_3` int(10) NOT NULL,
  `Formacion_Jugador_Local_3` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_3` int(10) NOT NULL,
  `Id_Jugador_Local_4` int(10) NOT NULL,
  `Formacion_Jugador_Local_4` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_4` int(10) NOT NULL,
  `Id_Jugador_Local_5` int(10) NOT NULL,
  `Formacion_Jugador_Local_5` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_5` int(10) NOT NULL,
  `Id_Jugador_Local_6` int(10) NOT NULL,
  `Formacion_Jugador_Local_6` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_6` int(10) NOT NULL,
  `Id_Jugador_Local_7` int(10) NOT NULL,
  `Formacion_Jugador_Local_7` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_7` int(10) NOT NULL,
  `Id_Jugador_Local_8` int(10) NOT NULL,
  `Formacion_Jugador_Local_8` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_8` int(10) NOT NULL,
  `Id_Jugador_Local_9` int(10) NOT NULL,
  `Formacion_Jugador_Local_9` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_9` int(10) NOT NULL,
  `Id_Jugador_Local_10` int(10) NOT NULL,
  `Formacion_Jugador_Local_10` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_10` int(10) NOT NULL,
  `Id_Jugador_Local_11` int(10) NOT NULL,
  `Formacion_Jugador_Local_11` varchar(25) NOT NULL,
  `Titularidad_Jugador_Local_11` int(10) NOT NULL,
  `Id_Jugador_Local_12` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_12` int(11) NOT NULL,
  `Id_Jugador_Local_13` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_13` int(11) NOT NULL,
  `Id_Jugador_Local_14` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_14` int(11) NOT NULL,
  `Id_Jugador_Local_15` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_15` int(11) NOT NULL,
  `Id_Jugador_Local_16` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_16` int(11) NOT NULL,
  `Id_Jugador_Local_17` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_17` int(11) NOT NULL,
  `Id_Jugador_Local_18` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_18` int(11) NOT NULL,
  `Id_Jugador_Local_19` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_19` int(11) NOT NULL,
  `Id_Jugador_Local_20` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_20` int(11) NOT NULL,
  `Id_Jugador_Local_21` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_21` int(11) NOT NULL,
  `Id_Jugador_Local_22` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_22` int(11) NOT NULL,
  `Id_Jugador_Local_23` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_23` int(11) NOT NULL,
  `Id_Jugador_Local_24` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_24` int(11) NOT NULL,
  `Id_Jugador_Local_25` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Local_25` int(11) NOT NULL,
  `Id_Jugador_Visitante_1` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_1` varchar(25) NOT NULL,
  `Titularidad_Jugador_Visitante_1` int(10) NOT NULL,
  `Id_Jugador_Visitante_2` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_2` varchar(25) NOT NULL,
  `Titularidad_Jugador_Visitante_2` int(10) NOT NULL,
  `Id_Jugador_Visitante_3` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_3` varchar(25) NOT NULL,
  `Titularidad_Jugador_Visitante_3` int(10) NOT NULL,
  `Id_Jugador_Visitante_4` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_4` varchar(25) NOT NULL,
  `Titularidad_Jugador_Visitante_4` int(10) NOT NULL,
  `Id_Jugador_Visitante_5` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_5` varchar(25) NOT NULL,
  `Titularidad_Jugador_Visitante_5` int(10) NOT NULL,
  `Id_Jugador_Visitante_6` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_6` varchar(25) NOT NULL,
  `Titularidad_Jugador_Visitante_6` int(10) NOT NULL,
  `Id_Jugador_Visitante_7` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_7` varchar(27) NOT NULL,
  `Titularidad_Jugador_Visitante_7` int(10) NOT NULL,
  `Id_Jugador_Visitante_8` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_8` varchar(27) NOT NULL,
  `Titularidad_Jugador_Visitante_8` int(10) NOT NULL,
  `Id_Jugador_Visitante_9` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_9` varchar(25) NOT NULL,
  `Titularidad_Jugador_Visitante_9` int(10) NOT NULL,
  `Id_Jugador_Visitante_10` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_10` varchar(25) NOT NULL,
  `Titularidad_Jugador_Visitante_10` int(10) NOT NULL,
  `Id_Jugador_Visitante_11` int(10) NOT NULL,
  `Formacion_Jugador_Visitante_11` varchar(25) NOT NULL,
  `Titularidad_Jugador_Visitante_11` int(10) NOT NULL,
  `Id_Jugador_Visitante_12` int(10) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_12` int(10) NOT NULL,
  `Id_Jugador_Visitante_13` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_13` int(11) NOT NULL,
  `Id_Jugador_Visitante_14` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_14` int(11) NOT NULL,
  `Id_Jugador_Visitante_15` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_15` int(11) NOT NULL,
  `Id_Jugador_Visitante_16` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_16` int(11) NOT NULL,
  `Id_Jugador_Visitante_17` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_17` int(11) NOT NULL,
  `Id_Jugador_Visitante_18` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_18` int(11) NOT NULL,
  `Id_Jugador_Visitante_19` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_19` int(11) NOT NULL,
  `Id_Jugador_Visitante_20` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_20` int(11) NOT NULL,
  `Id_Jugador_Visitante_21` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_21` int(11) NOT NULL,
  `Id_Jugador_Visitante_22` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_22` int(11) NOT NULL,
  `Id_Jugador_Visitante_23` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_23` int(11) NOT NULL,
  `Id_Jugador_Visitante_24` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_24` int(11) NOT NULL,
  `Id_Jugador_Visitante_25` int(11) NOT NULL,
  `Minuto_Sustitucion_Jugador_Visitante_25` int(11) NOT NULL,
  `Observaciones` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `Id_Equipo` int(10) NOT NULL,
  `Nombre_Equipo` varchar(50) NOT NULL,
  `Id_PaisF` int(10) NOT NULL,
  `Id_EstadoF` int(10) NOT NULL,
  `Id_Municipio` int(10) NOT NULL,
  `Id_localidad` int(10) NOT NULL,
  `Id_categoriaF` int(10) NOT NULL,
  `N_Jugadores` int(10) NOT NULL,
  `Id_TecnicoF` int(10) NOT NULL,
  `Fundacion` date NOT NULL,
  `Goles` int(10) NOT NULL,
  `Asistencia` int(10) NOT NULL,
  `Tamarilla` int(10) NOT NULL,
  `Trojas` int(10) NOT NULL,
  `Estadio` varchar(25) NOT NULL,
  `Ubicacion` varchar(25) NOT NULL,
  `Url_Bandera` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada_resultado`
--

CREATE TABLE `jornada_resultado` (
  `Id` int(11) NOT NULL,
  `Id_Jornada` int(11) NOT NULL,
  `Id_Liga` int(11) NOT NULL,
  `Id_Categoria` int(11) NOT NULL,
  `Temporada` varchar(25) NOT NULL,
  `Id_Torneo` int(11) NOT NULL,
  `Id_Encuentro` int(11) NOT NULL,
  `Fecha_Encuentro` date NOT NULL,
  `Hora_Encuentro` time NOT NULL,
  `Id_Equipo_local` int(11) NOT NULL,
  `Nombre_Equipo_local` varchar(50) NOT NULL,
  `Score_Equipo_local` int(11) NOT NULL,
  `Escudo_Equipo_local` varchar(150) NOT NULL,
  `Id_Equipo_Visitante` int(11) NOT NULL,
  `Nombre_Equipo_Visitante` varchar(50) NOT NULL,
  `Score_Equipo_Visitante` int(11) NOT NULL,
  `Escudo_Equipo_Visitante` varchar(150) NOT NULL,
  `Status_Encuentro` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `jornada_resultado`
--

INSERT INTO `jornada_resultado` (`Id`, `Id_Jornada`, `Id_Liga`, `Id_Categoria`, `Temporada`, `Id_Torneo`, `Id_Encuentro`, `Fecha_Encuentro`, `Hora_Encuentro`, `Id_Equipo_local`, `Nombre_Equipo_local`, `Score_Equipo_local`, `Escudo_Equipo_local`, `Id_Equipo_Visitante`, `Nombre_Equipo_Visitante`, `Score_Equipo_Visitante`, `Escudo_Equipo_Visitante`, `Status_Encuentro`) VALUES
(1, 1, 3, 1, '2018-2019', 1, 1, '2019-06-24', '17:00:00', 1, 'Emelec', 2, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/1.png', 2, 'Barcelona', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/2.png', 'Closed'),
(2, 1, 3, 1, '2018-2019', 1, 2, '2019-06-24', '18:00:00', 3, 'Delfin', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/3.png', 4, 'Liga Universitaria', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/4.png', 'Closed'),
(3, 1, 3, 1, '2018-2019', 1, 3, '2019-06-27', '13:00:00', 5, 'Aucas', 3, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/5.png', 6, 'Nacional', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/6.png', 'Pendiente'),
(4, 1, 3, 1, '2018-2019', 1, 4, '2019-06-25', '05:30:00', 7, 'Macara', 0, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/7.png', 8, 'Deportivo olmedo', 0, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/8.png', 'Finalizado'),
(5, 8, 3, 1, '2018-2019', 1, 5, '2019-06-25', '13:00:00', 9, 'Deportivo Cuenca', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/9.png', 10, 'Independiente del Valle', 2, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/10.png', 'Finalizado'),
(6, 1, 3, 1, '2018-2019', 1, 6, '2019-06-27', '08:00:00', 11, 'Fuerza Amarilla', 2, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/11.png', 12, 'Guayaquil City', 2, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/12.png', 'Pendiente'),
(7, 8, 3, 1, '2017-2018', 1, 7, '2019-06-26', '17:15:00', 13, 'Deportivo America ', 3, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/13.png', 14, 'Mushuc Runa Sporting', 4, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/14.png', 'Finalizado'),
(8, 2, 3, 1, '2018-2019', 1, 15, '2019-06-26', '13:00:00', 2, 'Barcelona', 1, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/2.png', 9, 'Deportivo Cuenca', 3, 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/9.png', 'Pendiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada_torneo`
--

CREATE TABLE `jornada_torneo` (
  `Id` int(11) NOT NULL,
  `Id_Liga` int(11) NOT NULL,
  `Id_categoria` int(11) NOT NULL,
  `Temporada` varchar(25) NOT NULL,
  `Id_Torneo` int(11) NOT NULL,
  `Jornada_numero` int(11) NOT NULL,
  `Desde` date NOT NULL,
  `Hasta` date NOT NULL,
  `Encuentros_Disputados_Jornada` int(11) NOT NULL,
  `Status_jornada` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `jornada_torneo`
--

INSERT INTO `jornada_torneo` (`Id`, `Id_Liga`, `Id_categoria`, `Temporada`, `Id_Torneo`, `Jornada_numero`, `Desde`, `Hasta`, `Encuentros_Disputados_Jornada`, `Status_jornada`) VALUES
(1, 1, 1, '2018-2019', 1, 1, '2018-08-17', '2018-08-20', 5, 'Closed'),
(2, 1, 1, '2018-2019', 1, 2, '2018-08-24', '2018-08-27', 6, 'Closed'),
(3, 1, 1, '2018-2019', 1, 3, '2018-08-31', '2018-09-24', 5, 'Closed'),
(4, 1, 1, '2018-2019', 1, 4, '2018-09-14', '2018-09-17', 6, 'Closed'),
(5, 1, 1, '2018-2019', 1, 5, '2019-07-01', '2019-07-01', 85, 'Open'),
(6, 1, 1, '2018-2019', 0, 0, '0000-00-00', '0000-00-00', 0, ''),
(7, 1, 1, '', 0, 0, '0000-00-00', '0000-00-00', 0, ''),
(8, 1, 1, '', 0, 0, '0000-00-00', '0000-00-00', 0, ''),
(10, 1, 1, '', 0, 0, '0000-00-00', '0000-00-00', 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jugador`
--

CREATE TABLE `jugador` (
  `Id_Jugador` int(11) NOT NULL,
  `Registro_Jugador` varchar(25) NOT NULL,
  `Nombre_Jugador` varchar(50) NOT NULL,
  `Apellido_Jugador` varchar(50) NOT NULL,
  `Edad_Jugador` int(2) NOT NULL,
  `Fecha_nacimiento_Jugador` date NOT NULL,
  `Lugar_Nacimiento` varchar(50) NOT NULL,
  `Nacionalidad_Jugador` varchar(30) NOT NULL,
  `Perfil_Jugador` varchar(25) NOT NULL,
  `Peso_Jugador` varchar(10) NOT NULL,
  `Altura_Jugador` varchar(10) NOT NULL,
  `Posicion_Jugador` varchar(25) NOT NULL,
  `Numero_Dorsal_Jugador` int(3) NOT NULL,
  `record_JJ_Jugador` int(10) NOT NULL,
  `record_JG_Jugador` int(10) NOT NULL,
  `record_JE_Jugador` int(10) NOT NULL,
  `record_JP_Jugador` int(10) NOT NULL,
  `record_Minutos_Jugador` varchar(10) NOT NULL,
  `record_Goles_jugador` int(10) NOT NULL,
  `record_Asistencia_Jugador` int(10) NOT NULL,
  `record_T_amarilla_Jugador` int(10) NOT NULL,
  `record_T_roja_Jugador` int(10) NOT NULL,
  `record_DisparosPuerta_Jugador` int(10) NOT NULL,
  `record_Ranking_Jugador` decimal(10,2) NOT NULL,
  `Id_EquipoActual_Jugador` int(10) NOT NULL,
  `Name_EquipoActual_Jugador` varchar(50) NOT NULL,
  `Url_Foto_Perfil` varchar(150) NOT NULL,
  `Pais` varchar(50) NOT NULL,
  `Liga` varchar(50) NOT NULL,
  `Categoria` varchar(50) NOT NULL,
  `adddate` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `liga`
--

CREATE TABLE `liga` (
  `Id_Liga` int(11) NOT NULL,
  `Nombre_Liga` varchar(50) NOT NULL,
  `Id_PaisF` int(10) NOT NULL,
  `paisnameF` varchar(100) NOT NULL,
  `Id_EstadoF` int(10) NOT NULL,
  `EstadonameF` varchar(100) NOT NULL,
  `Id_MunicipioF` int(10) NOT NULL,
  `MunicipioNameF` varchar(100) NOT NULL,
  `Id_Parroquia` int(10) NOT NULL,
  `ParroquiaNameF` varchar(100) NOT NULL,
  `Total_Categorias` int(10) NOT NULL,
  `Total_Equipos` int(10) NOT NULL,
  `Total_Jugadores` int(10) NOT NULL,
  `Descripcion` text NOT NULL,
  `paisname` varchar(25) NOT NULL,
  `banderapais` varchar(200) NOT NULL,
  `Secretario` varchar(25) NOT NULL,
  `Tlf1` varchar(25) NOT NULL,
  `Tlf2` varchar(25) NOT NULL,
  `Url_Bandera` varchar(150) NOT NULL,
  `Fundacion` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `liga`
--

INSERT INTO `liga` (`Id_Liga`, `Nombre_Liga`, `Id_PaisF`, `paisnameF`, `Id_EstadoF`, `EstadonameF`, `Id_MunicipioF`, `MunicipioNameF`, `Id_Parroquia`, `ParroquiaNameF`, `Total_Categorias`, `Total_Equipos`, `Total_Jugadores`, `Descripcion`, `paisname`, `banderapais`, `Secretario`, `Tlf1`, `Tlf2`, `Url_Bandera`, `Fundacion`) VALUES
(0, 'Liga Barrial La Argelia', 2, 'Ecuador', 2, 'Pichincha', 1, 'DM-Quito', 1, 'Conocoto', 7, 17, 250, 'Segunda liga test', '', 'http://localhost/kornner-360/v1/Img/Bandera/ecuador.png', '', '', '', 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/2.png', ''),
(1, 'Liga Barrial Santa Lucia', 2, 'Ecuador', 2, 'Pichincha', 1, 'DM-Quito', 2, 'Calderon', 5, 15, 300, 'Sembrando Jugadores en una Liga Demo', 'Ecuador', 'http://localhost/kornner-360/v1/Img/Bandera/ecuador.png', '', '', '', 'http://localhost/kornner-360/v1/360/Img/Clubes/Ecuador/Liga-Pro/Icono/1.png', '2012'),
(33, '', 3, 'Peru', 5, '', 0, '', 0, '', 0, 0, 0, '', '', '', '', '', '', '', ''),
(36, 'Liga_Demo', 3, 'Peru', 5, '', 0, '', 0, '', 0, 0, 0, '', 'Peru', '', '', '', '', '', ''),
(45, '', 3, 'Peru', 5, '', 0, '', 0, '', 0, 0, 0, '', '', '', '', '', '', '', ''),
(46, '', 2, '', 1, '', 0, '', 0, '', 0, 0, 0, '', '', '', '', '', '', '', ''),
(55, '', 2, '', 3, '', 0, '', 0, '', 0, 0, 0, '', '', '', '', '', '', '', ''),
(63, '', 2, '', 4, '', 0, '', 0, '', 0, 0, 0, '', '', '', '', '', '', '', ''),
(65, '', 2, '', 4, '', 0, '', 0, '', 0, 0, 0, '', '', '', '', '', '', '', ''),
(76, '', 2, '', 4, '', 0, '', 0, '', 0, 0, 0, '', '', '', '', '', '', '', ''),
(78, '', 4, '', 6, '', 0, '', 0, '', 0, 0, 0, '', 'Francia', '', '', '', '', '', ''),
(79, '', 4, 'Francia', 9, '', 0, '', 0, '', 0, 0, 0, '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `record_encuentro_asistencias`
--

CREATE TABLE `record_encuentro_asistencias` (
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `record_encuentro_disparos`
--

CREATE TABLE `record_encuentro_disparos` (
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `record_encuentro_faltas`
--

CREATE TABLE `record_encuentro_faltas` (
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `record_encuentro_gol`
--

CREATE TABLE `record_encuentro_gol` (
  `Id` int(11) NOT NULL,
  `Id_Encuentro` int(10) NOT NULL,
  `Date_Encuentro` date NOT NULL,
  `Temporada` varchar(10) NOT NULL,
  `Id_LigaF` int(10) NOT NULL,
  `Id_TorneoF` int(10) NOT NULL,
  `Id_Jugador` int(10) NOT NULL,
  `Jugador_Name` int(10) NOT NULL,
  `Anoto_As` varchar(25) NOT NULL,
  `Minuto_Gol` time NOT NULL,
  `Type_gol` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `record_jugador_torneo`
--

CREATE TABLE `record_jugador_torneo` (
  `Id` int(11) NOT NULL,
  `Temporada` varchar(10) NOT NULL,
  `Id_LigaF` varchar(25) NOT NULL,
  `Nombre_Liga` varchar(50) NOT NULL,
  `Id_TorneoF` varchar(25) NOT NULL,
  `Nombre_Torneo` varchar(50) NOT NULL,
  `Id_categoriaF` int(11) NOT NULL,
  `Nombre_Categoria` varchar(50) NOT NULL,
  `Id_JugadorF` int(11) NOT NULL,
  `Nombre_JugadorF` varchar(50) NOT NULL,
  `Id_Equipo` int(10) NOT NULL,
  `Nombre_Equipo` varchar(50) NOT NULL,
  `JJ` int(11) NOT NULL,
  `JG` int(11) NOT NULL,
  `JE` int(11) NOT NULL,
  `JP` int(11) NOT NULL,
  `Encuentros_Titular` int(11) NOT NULL,
  `Encuentros_Suplente` int(11) NOT NULL,
  `Minutos_Jugados` time NOT NULL,
  `Minutos_Titular` time NOT NULL,
  `Minutos_suplentes` time NOT NULL,
  `Goles_Totales` int(11) NOT NULL,
  `Goles_Titular` int(11) NOT NULL,
  `Goles_Suplente` int(11) NOT NULL,
  `Asistencias_Totales` int(11) NOT NULL,
  `Asistencias_Titular` int(11) NOT NULL,
  `Asistencias_Suplente` int(11) NOT NULL,
  `Porcentaje_Minuto_Gol` float NOT NULL,
  `Total_Tarjetas` int(11) NOT NULL,
  `Tarjetas_Amarillas` int(11) NOT NULL,
  `Tarjetas_Rojas` int(3) NOT NULL,
  `Suspenciones` int(5) NOT NULL,
  `Habilitado` int(1) NOT NULL,
  `Motivo` varchar(100) NOT NULL,
  `Url_Foto_Perfil` varchar(150) NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `record_jugador_torneo`
--

INSERT INTO `record_jugador_torneo` (`Id`, `Temporada`, `Id_LigaF`, `Nombre_Liga`, `Id_TorneoF`, `Nombre_Torneo`, `Id_categoriaF`, `Nombre_Categoria`, `Id_JugadorF`, `Nombre_JugadorF`, `Id_Equipo`, `Nombre_Equipo`, `JJ`, `JG`, `JE`, `JP`, `Encuentros_Titular`, `Encuentros_Suplente`, `Minutos_Jugados`, `Minutos_Titular`, `Minutos_suplentes`, `Goles_Totales`, `Goles_Titular`, `Goles_Suplente`, `Asistencias_Totales`, `Asistencias_Titular`, `Asistencias_Suplente`, `Porcentaje_Minuto_Gol`, `Total_Tarjetas`, `Tarjetas_Amarillas`, `Tarjetas_Rojas`, `Suspenciones`, `Habilitado`, `Motivo`, `Url_Foto_Perfil`, `IsActive`) VALUES
(1, '2018-2019', '1', 'Liga_Demo', '1', 'Torneo_Demo', 1, 'Categoria_Demo', 1, 'Miguel Angel Moya', 1, 'Real Sociedad de Fútbol', 24, 15, 5, 4, 22, 2, '00:44:35', '10:30:46', '12:26:34', 17, 15, 2, 12, 10, 2, 93, 2, 2, 0, 0, 0, '', 'Liga/Imagenes/Jugadores/1.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla_posiciones`
--

CREATE TABLE `tabla_posiciones` (
  `Id` int(11) NOT NULL,
  `Id_Liga` int(25) NOT NULL,
  `Nombre_Liga` varchar(50) NOT NULL,
  `Id_Categoria` int(25) NOT NULL,
  `Nombre_Categoria` varchar(50) NOT NULL,
  `Temporada` varchar(25) NOT NULL,
  `Id_Equipo` int(25) NOT NULL,
  `Nombre_Equipo` varchar(50) NOT NULL,
  `JJ` int(5) NOT NULL,
  `JG` int(5) NOT NULL,
  `JE` int(5) NOT NULL,
  `JP` int(5) NOT NULL,
  `GF` int(5) NOT NULL,
  `GC` int(5) NOT NULL,
  `GD` int(5) NOT NULL,
  `Pts` int(10) NOT NULL,
  `Racha` varchar(10) NOT NULL,
  `Total_Tamarilla` int(11) NOT NULL,
  `Total_Troja` int(11) NOT NULL,
  `Total_Tarjetas` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL,
  `ImagenEscudo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tabla_posiciones`
--

INSERT INTO `tabla_posiciones` (`Id`, `Id_Liga`, `Nombre_Liga`, `Id_Categoria`, `Nombre_Categoria`, `Temporada`, `Id_Equipo`, `Nombre_Equipo`, `JJ`, `JG`, `JE`, `JP`, `GF`, `GC`, `GD`, `Pts`, `Racha`, `Total_Tamarilla`, `Total_Troja`, `Total_Tarjetas`, `IsActive`, `ImagenEscudo`) VALUES
(1, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2018-2019', 1, 'Barcelona', 38, 26, 9, 3, 90, 36, 54, 87, '2 Ganados', 0, 0, 0, 1, 'http://localhost/kornner-360/v2/360/Liga/Clubes/Espana/BBVA/Icono/3.png'),
(2, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2018-2019', 2, 'Atletico Madrid', 38, 22, 10, 6, 55, 29, 26, 76, '2 Ganados', 0, 0, 0, 1, 'http://localhost/kornner-360/v2/360/Liga/Clubes/Espana/BBVA/Icono/42.png'),
(3, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2018-2019', 3, 'Real Madrid', 38, 21, 5, 12, 63, 46, 17, 68, '1 Empates', 0, 0, 0, 1, 'http://localhost/kornner-360/v2/360/Liga/Clubes/Espana/BBVA/Icono/1.png'),
(4, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2018-2019', 4, 'Valencia C.F.', 38, 15, 16, 7, 51, 35, 16, 61, '2 Perdidos', 8, 3, 11, 1, 'http://localhost/kornner-360/v2/360/Liga/Clubes/Espana/BBVA/Icono/17.png'),
(5, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2018-2019', 5, 'Gatafe', 38, 15, 14, 9, 48, 35, 13, 59, '1 Empates', 0, 0, 0, 1, 'http://localhost/kornner-360/v2/360/Liga/Clubes/Espana/BBVA/Icono/7.png'),
(6, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2018-2019', 6, 'Sevilla', 38, 17, 8, 13, 62, 47, 15, 59, '1 Ganados', 0, 0, 0, 1, 'http://localhost/kornner-360/v2/360/Liga/Clubes/Espana/BBVA/Icono/53_sevilla.png'),
(7, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2018-2019', 7, 'RCD Espanyol', 38, 14, 11, 13, 48, 50, -2, 53, '2 Empates', 0, 0, 0, 1, 'http://localhost/kornner-360/v2/360/Liga/Clubes/Espana/BBVA/Icono/8.png'),
(8, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2018-2019', 8, 'Ath.Bilbao', 38, 13, 14, 11, 41, 45, -4, 53, '1 Ganados', 0, 0, 0, 1, 'http://localhost/kornner-360/v2/360/Liga/Clubes/Espana/BBVA/Icono/5.png'),
(9, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2018-2019', 9, 'Real Sociedad', 38, 13, 11, 14, 45, 46, -1, 50, '1 Perdidos', 0, 0, 0, 1, 'http://localhost/kornner-360/v2/360/Liga/Clubes/Espana/BBVA/Icono/16.png'),
(10, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2018-2019', 10, 'Betis', 38, 14, 8, 16, 44, 52, -8, 50, '1 Perdidos', 0, 0, 0, 1, 'http://localhost/kornner-360/v2/360/Liga/Clubes/Espana/BBVA/Icono/15.png'),
(11, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2017-2018', 1, 'Barcelona', 38, 28, 9, 1, 99, 29, 70, 93, '1 Ganados', 0, 0, 0, 0, 'Imagenes/Escudo/Barcelona.png'),
(12, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2017-2018', 2, 'Atletico Madrid', 38, 23, 10, 5, 58, 22, 36, 79, '1 Empates', 0, 0, 0, 0, 'Imagenes/Escudo/Atletico Madrid.png'),
(13, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2017-2018', 3, 'Real Madrid', 38, 22, 10, 6, 94, 44, 50, 76, '1 Ganados', 0, 0, 0, 0, 'Imagenes/Escudo/Real Madrid.png'),
(14, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2017-2018', 4, 'Valencia C.F.', 38, 22, 7, 9, 65, 38, 27, 73, '2 Empates', 15, 7, 22, 0, 'Imagenes/Escudo/Valencia.jpg'),
(15, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2017-2018', 11, 'Villarreal', 38, 18, 7, 13, 57, 50, 7, 61, '2 Ganados', 0, 0, 0, 0, 'Imagenes/Escudo/Villarreal.png'),
(16, 1, 'Liga_Demo', 1, 'Categoria_Demo', '2017-2018', 10, 'Betis', 38, 18, 6, 14, 60, 61, -1, 60, '1 Empates', 0, 0, 0, 0, 'Imagenes/Escudo/Betis.jpg'),
(17, 2, 'Premier League', 2, 'Primera Division', '2018-2019', 12, 'Manchester City', 38, 32, 2, 4, 95, 23, 72, 98, '5 Ganados', 0, 0, 0, 1, ''),
(18, 2, 'Premier League', 2, 'Primera Division', '2018-2019', 13, 'Liverpool', 38, 30, 7, 1, 89, 22, 67, 97, '5 Ganados', 0, 0, 0, 1, ''),
(19, 2, 'Premier League', 2, 'Primera Division', '2018-2019', 14, 'Chelsea', 38, 21, 9, 8, 63, 39, 24, 72, '1 Empates', 0, 0, 0, 1, ''),
(20, 2, 'Premier League', 2, 'Primera Division', '2017-2018', 12, 'Manchester City', 38, 32, 4, 2, 106, 27, 79, 100, '2 Ganados', 0, 0, 0, 0, 'Imagenes/Escudo'),
(21, 2, 'Premier League', 2, 'Primera Division', '2017-2018', 15, 'Manchester United', 38, 25, 6, 7, 68, 28, 40, 81, '1 Ganados', 0, 0, 0, 0, 'Imagenes/Escudo'),
(22, 2, 'Premier League', 2, 'Primera Division', '2017-2018', 16, 'Tottenham Hotspur', 38, 23, 8, 7, 74, 36, 38, 77, '2 Ganados', 0, 0, 0, 0, ''),
(23, 2, 'Premier League', 2, 'Primera Division', '2017-2018', 17, 'Liverpool', 38, 21, 12, 5, 84, 38, 46, 75, '1 Ganados', 0, 0, 0, 0, ''),
(24, 3, 'Liga Pro', 3, 'Maxima', '2018-2019', 17, 'Barcelona SC', 15, 10, 3, 2, 34, 16, 18, 33, '5 Ganados', 0, 0, 0, 1, ''),
(25, 3, 'Liga Pro', 3, 'Maxima', '2018-2019', 18, 'Macara', 15, 9, 5, 1, 25, 8, 17, 32, '1 Empate', 0, 0, 0, 1, ''),
(26, 3, 'Liga Pro', 3, 'Maxima', '2017-2018', 19, 'EMELEC', 22, 12, 5, 5, 35, 17, 18, 41, '2 Ganados', 0, 0, 0, 0, ''),
(27, 3, 'Liga Pro', 3, 'Maxima', '2017-2018', 18, 'Macara', 22, 10, 8, 4, 31, 24, 7, 38, '1 Perdido', 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjeta`
--

CREATE TABLE `tarjeta` (
  `Id` int(11) NOT NULL,
  `Temporada` varchar(10) NOT NULL,
  `Id_Torneo` varchar(25) NOT NULL,
  `Torneo_Name` varchar(25) NOT NULL,
  `Id_Equipo` varchar(25) NOT NULL,
  `Equipo_Name` varchar(25) NOT NULL,
  `Id_Jugador` varchar(25) NOT NULL,
  `Jugador_Name` varchar(25) NOT NULL,
  `Tarjetas_Amarillas` int(5) NOT NULL,
  `Tarjetas_Rojas` int(5) NOT NULL,
  `Total_Tarjetas` int(5) NOT NULL,
  `IsActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temporada`
--

CREATE TABLE `temporada` (
  `Id` int(11) NOT NULL,
  `Temporada` varchar(10) NOT NULL,
  `Id_LigaF` int(10) NOT NULL,
  `Liga_Name` varchar(50) NOT NULL,
  `IsActive` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `torneo`
--

CREATE TABLE `torneo` (
  `Id_Torneo` int(11) NOT NULL,
  `Torneo_Name` varchar(50) NOT NULL,
  `Temporada` varchar(25) NOT NULL,
  `Id_LigaF` int(10) NOT NULL,
  `Liga_Name` varchar(50) NOT NULL,
  `Id_categoriaF` int(10) NOT NULL,
  `Name_Categoria` varchar(50) NOT NULL,
  `Total_Equipos_Participantes` int(11) NOT NULL,
  `Total_Jugadores_Participantes` int(11) NOT NULL,
  `Id_Equipo_Campeon` int(10) NOT NULL,
  `Nombre_Campeon` varchar(50) NOT NULL,
  `Id_Equipo_Sub-Campeon` int(10) NOT NULL,
  `nombre_Subcampeon` varchar(50) NOT NULL,
  `Url_Logo` varchar(150) NOT NULL,
  `Is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `torneo`
--

INSERT INTO `torneo` (`Id_Torneo`, `Torneo_Name`, `Temporada`, `Id_LigaF`, `Liga_Name`, `Id_categoriaF`, `Name_Categoria`, `Total_Equipos_Participantes`, `Total_Jugadores_Participantes`, `Id_Equipo_Campeon`, `Nombre_Campeon`, `Id_Equipo_Sub-Campeon`, `nombre_Subcampeon`, `Url_Logo`, `Is_active`) VALUES
(1, '', '2015-2016', 4, '', 6, '', 0, 0, 3, 'Juventus', 1, 'Chelsea', '', 0),
(2, '', '2016-2017', 4, '', 6, '', 0, 0, 1, 'Chelsea', 2, 'Barcelona', '', 0),
(4, '', '2017-2018', 4, '', 6, '', 0, 0, 3, 'Juventus', 2, 'Barcelona', '', 0),
(5, '', '2018-2019', 4, '', 6, '', 0, 0, 0, '', 0, '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_estado`
--

CREATE TABLE `t_estado` (
  `id_estado` int(11) NOT NULL,
  `estado` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Id_PaisF` int(10) NOT NULL,
  `Url_Bandera` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `t_estado`
--

INSERT INTO `t_estado` (`id_estado`, `estado`, `Id_PaisF`, `Url_Bandera`) VALUES
(1, 'Guayas', 2, ''),
(2, 'Pichincha', 2, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_localidad`
--

CREATE TABLE `t_localidad` (
  `id_localidad` int(11) NOT NULL,
  `localidad` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `id_municipio` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Url_Bandera` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `t_localidad`
--

INSERT INTO `t_localidad` (`id_localidad`, `localidad`, `id_municipio`, `Url_Bandera`) VALUES
(1, 'calderon', '1', ''),
(2, 'conocoto', '1', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_municipio`
--

CREATE TABLE `t_municipio` (
  `id_municipio` int(11) NOT NULL,
  `municipio` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `id_estado` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Url_Bandera` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `t_municipio`
--

INSERT INTO `t_municipio` (`id_municipio`, `municipio`, `id_estado`, `Url_Bandera`) VALUES
(1, 'DM-Quito', '2', ''),
(2, 'Mejía', '2', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_pais`
--

CREATE TABLE `t_pais` (
  `Id_Pais` int(11) NOT NULL,
  `Nombre_Pais` varchar(50) NOT NULL,
  `Continente` varchar(50) NOT NULL,
  `Url_Bandera` varchar(150) NOT NULL,
  `adddate` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `t_pais`
--

INSERT INTO `t_pais` (`Id_Pais`, `Nombre_Pais`, `Continente`, `Url_Bandera`, `adddate`) VALUES
(1, 'Chile', 'America', 'http://localhost/kornner-360/V2/360/liga/pais/Chile/Bandera.png', ''),
(2, 'Ecuador', 'America', 'http://localhost/kornner-360/V2/360/liga/pais/Ecuador/Bandera.png', ''),
(3, 'Peru', 'America', 'http://localhost/kornner-360/V2/360/liga/pais/Peru/Bandera.png', ''),
(4, 'Francia', 'Europa', 'http://localhost/kornner-360/V2/360/liga/pais/Francia/Bandera.jpg', ''),
(5, 'Venezuela', 'America', 'http://localhost/kornner-360/V2/360/liga/pais/Venezuela/Bandera.png', ''),
(6, 'Italia', 'Europa', 'http://localhost/kornner-360/V2/360/liga/pais/Italia/Bandera.png', ''),
(7, 'Mexico', 'America', 'http://localhost/kornner-360/V2/360/liga/pais/Mexico/Bandera.png', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `encuentro`
--
ALTER TABLE `encuentro`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `jornada_resultado`
--
ALTER TABLE `jornada_resultado`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `jornada_torneo`
--
ALTER TABLE `jornada_torneo`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `liga`
--
ALTER TABLE `liga`
  ADD PRIMARY KEY (`Id_Liga`);

--
-- Indices de la tabla `record_encuentro_asistencias`
--
ALTER TABLE `record_encuentro_asistencias`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `record_encuentro_disparos`
--
ALTER TABLE `record_encuentro_disparos`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `record_encuentro_faltas`
--
ALTER TABLE `record_encuentro_faltas`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `record_encuentro_gol`
--
ALTER TABLE `record_encuentro_gol`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `record_jugador_torneo`
--
ALTER TABLE `record_jugador_torneo`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `tabla_posiciones`
--
ALTER TABLE `tabla_posiciones`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `temporada`
--
ALTER TABLE `temporada`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `t_estado`
--
ALTER TABLE `t_estado`
  ADD PRIMARY KEY (`id_estado`),
  ADD UNIQUE KEY `id_estado` (`id_estado`);

--
-- Indices de la tabla `t_localidad`
--
ALTER TABLE `t_localidad`
  ADD PRIMARY KEY (`id_localidad`),
  ADD UNIQUE KEY `id_localidad` (`id_localidad`);

--
-- Indices de la tabla `t_municipio`
--
ALTER TABLE `t_municipio`
  ADD PRIMARY KEY (`id_municipio`),
  ADD UNIQUE KEY `id_municipio` (`id_municipio`);

--
-- Indices de la tabla `t_pais`
--
ALTER TABLE `t_pais`
  ADD PRIMARY KEY (`Id_Pais`),
  ADD UNIQUE KEY `Id_Pais` (`Id_Pais`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `encuentro`
--
ALTER TABLE `encuentro`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `jornada_resultado`
--
ALTER TABLE `jornada_resultado`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `jornada_torneo`
--
ALTER TABLE `jornada_torneo`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `record_encuentro_asistencias`
--
ALTER TABLE `record_encuentro_asistencias`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `record_encuentro_disparos`
--
ALTER TABLE `record_encuentro_disparos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `record_encuentro_faltas`
--
ALTER TABLE `record_encuentro_faltas`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `record_jugador_torneo`
--
ALTER TABLE `record_jugador_torneo`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tabla_posiciones`
--
ALTER TABLE `tabla_posiciones`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `temporada`
--
ALTER TABLE `temporada`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `t_estado`
--
ALTER TABLE `t_estado`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `t_localidad`
--
ALTER TABLE `t_localidad`
  MODIFY `id_localidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `t_municipio`
--
ALTER TABLE `t_municipio`
  MODIFY `id_municipio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `t_pais`
--
ALTER TABLE `t_pais`
  MODIFY `Id_Pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
