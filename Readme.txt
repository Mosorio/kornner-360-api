Este proyecto solo contiene los endpoints para la API

Tres carpertas

Api-Client
Api-CMS
Api-Management

Version 1
	Contempla
		a) Api-Client
			* Pais
				- List-pasis
				- Info-Pais
			* Ligas
				- List-Liga 
				- Info-Liga
			* Temporadas
				- Get-temporadas
			* Categorias
				- Get-categoria
			* Noticias
				- Main
				- Noticias-Ligas
			* Torneo
				- Get-Torneo
				- Jornada
				- Goles-Torneo
				- Encuentros
				- Jugadores
				- Equipos
				- Tabla-Posiciones
				- Goleador
				- Ofensiva
				- Defensiva
				- Fair Play
			* Titulos
				- Campeones
				- MasTitulos